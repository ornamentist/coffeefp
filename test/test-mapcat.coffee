mapcat = require "../src/mapcat"
_      = require "../src/prelude"


describe "cat", ->
  it "should concatenate no arguments", ->
    mapcat.cat().should.eql []

  it "should concatenate one array", ->
    mapcat.cat([1, 2, 3]).should.eql [1, 2, 3]

  it "should concatenate multiple arrays", ->
    mapcat.cat([1,2], [3,4]).should.eql [1, 2, 3, 4]

  it "should concatenate multiple values", ->
    mapcat.cat(1, 2, 3, 4).should.eql [1, 2, 3, 4]

  it "should concatenate arrays and values", ->
    mapcat.cat([1,2], 3, 4).should.eql [1, 2, 3, 4]


describe "cons", ->
  it "should construct on missing array", ->
    mapcat.cons(42).should.eql [42]

  it "should construct on empty array", ->
    mapcat.cons(42, []).should.eql [42]

  it "should construct on singleton array", ->
    mapcat.cons(42, [43]).should.eql [42, 43]

  it "should construct arrays", ->
    mapcat.cons([42], [43, 44]).should.eql [[42], 43, 44]


describe "mapcat", ->
  commas = (e) -> mapcat.cons(e, [","])

  it "should map over missing array", ->
    mapcat.mapcat(_.identity).should.eql []

  it "should map over empty array", ->
    mapcat.mapcat(_.identity, []).should.eql []

  it "should map over array", ->
    mapcat.mapcat(_.identity, [1,2,3]).should.eql [1,2,3]

  it "should map function over array", ->
    mapcat.mapcat(commas, [1,2]).should.eql [1,",",2,","]


describe "interpose", ->
  it "should interpose over missing array", ->
    mapcat.interpose(",").should.eql []

  it "should interpose over empty array", ->
    mapcat.interpose(",", []).should.eql []

  it "should interpose over singleton array", ->
    mapcat.interpose(",", [42]).should.eql [42]

  it "should interpose over array", ->
    mapcat.interpose(",", [42,43]).should.eql [42,",",43]

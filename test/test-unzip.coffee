unzip = require "../src/unzip"


describe "cycle", ->
  it "should unzip zero times", ->
    unzip.cycle(0, [42]).should.eql []

  it "should unzip one times", ->
    unzip.cycle(1, [42]).should.eql [42]

  it "should unzip an empty array", ->
    unzip.cycle(3, []).should.eql []

  it "should unzip one element array", ->
    unzip.cycle(3, [42]).should.eql [42,42,42]

  it "should unzip two element array", ->
    unzip.cycle(2, [1,2]).should.eql [1,2,1,2]

  it "should unzip a nested array", ->
    unzip.cycle(2, [[42]]).should.eql [[42], [42]]


describe "pair", ->
  UNZIP  = [["a"], [1]]
  SINGLE = [["a", 2], [1, "b"]]

  it "should fail with empty pairs array", ->
    unzip.pair(["a", 1], []).should.not.eql UNZIP

  it "should fail with single pairs array", ->
    unzip.pair(["a", 1], [1]).should.not.eql UNZIP

  it "should make pairs with empty pairs", ->
    unzip.pair(["a", 1], [[], []]).should.eql UNZIP

  it "should make pairs with single pairs", ->
    unzip.pair(["a", 1], [[2], ["b"]]).should.eql SINGLE


describe "unzip", ->
  it "should detect empty zipped array", ->
    unzip.unzip([]).should.eql [[], []]

  it "should detect empty zipped entry", ->
    unzip.unzip([[]]).should.eql [[], []]

  it "should unzip single zipped entry", ->
    unzip.unzip([[1,2]]).should.eql [[1], [2]]

  it "should unzip zipped array", ->
    unzip.unzip([[1,2], [3,4]]).should.eql [[1,3], [2,4]]


queue = require "../src/queue"
_     = require "../src/prelude"


ITEMS = [1,2,3]
QUEUE = queue.queue(ITEMS)


describe "queue", ->
  it "should generate empty queue", ->
    queue.queue().should.have.length 0

  it "should generate 1 item queue", ->
    queue.queue([42]).should.have.length 1

  it "should generate queue", ->
    QUEUE.should.have.length 3


describe "enqueue", ->
  Q2 = queue.enqueue(QUEUE, 42)

  it "should add item", ->
    Q2.should.have.length 4

  it "should preserve source items", ->
    ITEMS.should.eql [1,2,3]



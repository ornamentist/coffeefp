find = require "../src/best"
_    = require "../src/prelude"


P1 = {name: "Fred", age: 65}
P2 = {name: "Lucy", age: 36}


describe "finder", ->
  older   = (p, q) -> p.age > q.age
  younger = (p, q) -> p.age < q.age

  it "should not find in missing arrays", ->
    (-> find.best(_.identity, _.identity)).should.raise

  it "should not find in empty arrays", ->
    (-> find.best(_.identity, _.identity)).should.raise

  it "should find maxima with objects", ->
    find.best(older, [P1, P2]).should.eql P1

  it "should find minima with objects", ->
    find.best(younger, [P1, P2]).should.eql P2

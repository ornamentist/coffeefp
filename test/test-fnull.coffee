fnull = require "../src/fnull"


describe "smultiply", ->
  it "should multiply 2 arguments", ->
    fnull.smultiply(2, 3).should.eql 6

  it "should detect null first argument", ->
    fnull.smultiply(null, 1).should.eql 1

  it "should detect null second argument", ->
    fnull.smultiply(1, null).should.eql 1

  it "should detect null arguments", ->
    fnull.smultiply(null, null).should.eql 1

  it "should fail with missing arguments", ->
    (-> fnull.smultiply()).should.raise

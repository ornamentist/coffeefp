freqs = require "../src/freqs"
_     = require "../src/prelude"


describe "frequencies", ->
  samples = [1,1,1,2,2,3,4]
  counts  = {1:3, 2:2, 3:1, 4:1}

  it "should summarize an empty array", ->
    freqs.frequencies([]).should.eql {}

  it "should summarize a singleton array", ->
    freqs.frequencies([42]).should.eql {42:1}

  it "should summarize an array", ->
    freqs.frequencies(samples).should.eql counts


describe "tosses", ->
  it "should summarize coin tosses", ->
    freqs.frequencies(freqs.tosses).should.have.keys('0', '1')



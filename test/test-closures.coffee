closure = require "../src/closures"


describe "pingpong", ->
  pp = closure.pingpong()

  it "should provide an inc function", ->
    pp.should.have.property "inc"

  it "should increment a number", ->
    pp.inc(42).should.eql(42)

  it "should provide a dec function", ->
    pp.should.have.property "dec"

  it "should decrement a number", ->
    pp.dec(43).should.eql(-1)

  it "should protect captured values", ->
    pp.div = (n) -> capture / n
    (-> pp.div(2)).should.raise


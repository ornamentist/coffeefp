conjoin = require "../src/conjoin"
_       = require "../src/prelude"


describe "all_even", ->
  it "should process no arguments", ->
    conjoin.all_even().should.be.true

  it "should process a single number", ->
    conjoin.all_even(42).should.be.true

  it "should process even numbers", ->
    conjoin.all_even(2,4,6,8).should.be.true

  it "should process mixed numbers", ->
    conjoin.all_even(1,2,3,4).should.be.false

  it "should process mixed types", ->
    conjoin.all_even(1,"2",{3:4}).should.be.false


describe "zero_odd", ->
  it "should process no arguments", ->
    conjoin.zero_odd().should.be.false

  it "should process a single number", ->
    conjoin.zero_odd(0).should.be.true

  it "should process odd numbers", ->
    conjoin.zero_odd(1,3,5,0).should.be.true

  it "should process mixed numbers", ->
    conjoin.zero_odd(0,1,2,3).should.be.true

  it "should process mixed types", ->
    conjoin.zero_odd(1,"2",{3:4}).should.be.true

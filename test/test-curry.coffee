curry = require "../src/curry"
_     = require "../src/prelude"


LOG = [
  {IP: "1.2.3.4", time: "1:2:3:4"},
  {IP: "4.3.2.1", time: "4:2:2:4"},
  {IP: "4.3.2.1", time: "4:2:2:4"},
  {IP: "1.2.3.4", time: "1:2:3:4"},
  {IP: "5.6.7.8", time: "1:9:8:8"}]


describe "count_IP", ->
  counts = curry.count_IP(LOG)

  it "should count by IP", ->
    _.pairs(counts).should.have.length 3



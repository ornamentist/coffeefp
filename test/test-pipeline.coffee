_    = require "../src/prelude"
pipe = require "../src/pipeline"


describe "pipeline", ->
  inc    = (n) -> n + 1
  dec    = (n) -> n - 1
  values = [2, 3, null, 4]

  it "should process empty pipelines", ->
    should.not.exist(pipe.pipeline())

  it "should process seed values", ->
    pipe.pipeline(42).should.eql 42

  it "should process local functions", ->
    pipe.pipeline(42, inc, dec).should.eql 42

  it "should process prelude functions", ->
    pipe.pipeline(values, _.compact, _.initial, _.rest).should.eql [3]

generator = require "../src/generator"
_         = require "../src/prelude"


describe "take", ->
  ints = generator.ints

  it "should generate zero integers", ->
    generator.take(0, ints).should.eql []

  it "should generate one integer", ->
    generator.take(1, ints).should.eql [0]

  it "should generate integers", ->
    generator.take(42, ints).should.have.length 42

partial = require "../src/partial"
_       = require "../src/prelude"


describe "over_10", ->
  it "should divide 0 by 10", ->
    partial.over_10(0).should.eql 0

  it "should divide by 10", ->
    partial.over_10(420).should.eql 42


describe "mult_over_10", ->
  it "should divide 0,0,0, by 10", ->
    partial.mult_over_10(0,0,0).should.eql 0

  it "should divide by 10", ->
    partial.mult_over_10(2,5,2).should.eql 2


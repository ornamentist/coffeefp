sql = require "../src/select"
_   = require "../src/prelude"


KEYS = ["title", "isbn", "ed"]


LIBRARY = [
  {title: "SICP", isbn: "0262010771", ed: 1}
  {title: "SICP", isbn: "0262510871", ed: 2}
  {title: "Joy",  isbn: "1935182641", ed: 1}
]


T_I = [
  {title: "SICP", isbn: "0262010771"}
  {title: "SICP", isbn: "0262510871"}
  {title: "Joy",  isbn: "1935182641"}
]


T_I_F = [
  {title: "SICP", isbn: "0262010771", foo: 1}
  {title: "SICP", isbn: "0262510871", foo: 2}
  {title: "Joy",  isbn: "1935182641", foo: 1}
]


describe "project", ->
  it "should not project on empty tables", ->
    sql.project([]).should.eql []

  it "should not project on missing keys", ->
    sql.project(LIBRARY).should.eql [{}, {}, {}]

  it "should not project on unknown keys", ->
    sql.project(LIBRARY, "foo").should.eql [{}, {}, {}]

  it "should project on all keys", ->
    sql.project(LIBRARY, KEYS).should.eql LIBRARY

  it "should project on known keys", ->
    sql.project(LIBRARY, "title", "isbn").should.eql T_I


describe "rename", ->
  R_1 = {title: "SICP", isbn: "0262010771", foo: 1}

  it "should not rename empty objects", ->
    sql.rename({}, foo: "foo").should.eql {}

  it "should not rename missing keys", ->
    sql.rename(LIBRARY[0], {}).should.eql LIBRARY[0]

  it "should not rename unknown keys", ->
    sql.rename(LIBRARY[0], foo: "foo").should.eql LIBRARY[0]

  it "should rename known keys", ->
    sql.rename(LIBRARY[0], ed: "foo").should.eql R_1


describe "as", ->
  it "should not rename empty tables", ->
    sql.as([], foo: "foo").should.eql []

  it "should not rename empty objects", ->
    sql.as([{}], foo: "foo").should.eql [{}]

  it "should not rename missing keys", ->
    sql.as(LIBRARY, {}).should.eql LIBRARY

  it "should not rename unknown keys", ->
    sql.as(LIBRARY, foo: "foo").should.eql LIBRARY

  it "should rename known keys", ->
    sql.as(LIBRARY, ed: "foo").should.eql T_I_F


describe "filter", ->
  e1 = (book) -> book.ed > 1
  e5 = (book) -> book.ed == 5

  it "should not filter empty tables", ->
    sql.restrict([], _.identity).should.eql []

  it "should find matching rows", ->
    sql.restrict(LIBRARY, e1).should.have.length 1

  it "should find non-matching rows", ->
    sql.restrict(LIBRARY, e5).should.eql []

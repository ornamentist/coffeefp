compose = require "../src/compose"
_       = require "../src/prelude"


describe "not_string", ->
  it "should detect missing value", ->
    compose.not_string().should.be.true

  it "should detect a number", ->
    compose.not_string(42).should.be.true

 it "should detect a string", ->
    compose.not_string("abc").should.be.false


describe "mapcat", ->
  it "should map empty array", ->
    compose.mapcat([], _.identity).should.eql []

it "should map simple array", ->
    compose.mapcat([1,2,3], _.identity).should.eql [1,2,3]

it "should map nested array", ->
    compose.mapcat([1,[2, 3]], _.identity).should.eql [1,2,3]

foldr = require "../src/foldr-all"


describe "all_of", ->
  it "should reduce no arguments", ->
    foldr.all_of().should.be.true

  it "should reduce 1 true argument", ->
    foldr.all_of(true).should.be.true

  it "should reduce 1 false argument", ->
    foldr.all_of(false).should.be.false

  it "should reduce mixed arguments", ->
    foldr.all_of(false, true, true).should.be.false


describe "any_of", ->
  it "should reduce no arguments", ->
    foldr.any_of().should.be.false

  it "should reduce 1 true argument", ->
    foldr.any_of(true).should.be.true

  it "should reduce 1 false argument", ->
    foldr.any_of(false).should.be.false

  it "should reduce mixed arguments", ->
    foldr.any_of(false, true, true).should.be.true

_ = require "../src/prelude"
a = require "../src/actions"


TWO   = [-4, ["result: 4", "result: -4"]]
THREE = [-9, ["result: 9", "result: -9"]]


describe "m_functions", ->
  it "`square` should return augmented result", ->
    a.m_square(3).should.eql [9, "result: 9"]

  it "`negate` should return augmented result", ->
    a.m_negate(3).should.eql [-3, "result: -3"]


describe "bind", ->
  it "should return a bound function", ->
    a.bind(a.m_square).should.be.a("function")

  it "should generate composable results", ->
    a.bind(a.m_square)([2,[]]).should.eql [4, ["result: 4"]]


describe "m_process", ->
  it "should return logged computations", ->
    a.m_process([2, []]).should.eql TWO

  it "should fail without arguments", ->
    (-> a.m_process()).should.raise


describe "unit", ->
  it "should unitize numbers", ->
    a.unit(3).should.eql [3, []]

  it "should enable m_process", ->
    a.m_process(a.unit(3)).should.eql THREE



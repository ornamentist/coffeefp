contain = require "../src/container"


add   = (n) -> n + 1
args  = (n, a, b) -> n + a + b


describe "container", ->
  beforeEach () ->
    @value = contain.container(42)

  it "should contain a value", ->
    @value.should.have.key "item"

  it "should preserve a value", ->
    @value.item.should.eql 42


describe "update", ->
  beforeEach () ->
    @value = contain.container(42)

  it "should update a value", ->
    contain.update(@value, add).item.should.eql 43

  it "should update arguments", ->
    contain.update(@value, args, 2, 3).item.should.eql 47


do_if = require "../src/do-if"


describe "do_if_field", ->
  it "should find array functions", ->
    do_if.do_if_field([1, 2, 3], "reverse").should.eql [3, 2, 1]

  it "should find object keys", ->
    do_if.do_if_field(foo: 42, "foo").should.eql 42

  it "should not find unknown object keys", ->
    (do_if.do_if_field([1, 2, 3], "baz") is undefined).should.be.true

  it "should not find unknown functions", ->
    (do_if.do_if_field([1, 2, 3], "foo") is undefined).should.be.true


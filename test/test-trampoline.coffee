trampoline = require "../src/trampoline"
_          = require "../src/prelude"


odd  = trampoline.odd
even = trampoline.even


describe "trampoline", ->
  it "should flatten zero calls", ->
    trampoline.trampoline(odd, 0).should.be.false

  it "should flatten one calls", ->
    trampoline.trampoline(even, 1).should.be.false

  it "should flatten number calls", ->
    trampoline.trampoline(odd, 42).should.be.false

  it "should flatten larger number calls", ->
    trampoline.trampoline(even, 4201).should.be.false

chain = require "../src/chain"


describe "Lazy", ->
  lazy  = new chain.Lazy([3,2,1])
  items = lazy.invoke("sort").invoke("join", "")

  it "should create objects", ->
    lazy.should.be.a "object"

  it "should provide invoking", ->
    lazy.should.have.property "invoke"

  it "should provide forcing", ->
    lazy.should.have.property "force"

  it "should work lazily", ->
    lazy.should.not.eql "123"

  it "should find values when needed", ->
    lazy.force().should.eql "123"


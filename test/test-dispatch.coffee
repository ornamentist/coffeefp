dispatch = require "../src/dispatch"
_        = require "../src/prelude"


describe "to-string", ->
  it "should stringize arrays", ->
    dispatch.to_string([1,2,3]).should.eql "1,2,3"

  it "should stringize strings", ->
    dispatch.to_string("foo").should.eql "foo"

  it "should not stringize numbers", ->
    should.not.exist(dispatch.to_string(123))


describe "reverse", ->
  it "should reverse arrays", ->
    dispatch.reverse([1,2,3]).should.eql [3,2,1]

  it "should reverse strings", ->
    dispatch.reverse("baz").should.eql "zab"

  it "should not reverse numbers", ->
    should.not.exist(dispatch.reverse(123))


describe "silly", ->
  it "should reverse arrays", ->
    dispatch.silly([1,2,3]).should.eql [3,2,1]

  it "should reverse strings", ->
    dispatch.silly("baz").should.eql "zab"

  it "should 42-ize numbers", ->
    dispatch.silly(123).should.eql 42

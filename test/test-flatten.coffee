flat = require "../src/flatten"
_    = require "../src/prelude"


describe "flatten", ->
  it "should flatten empty array", ->
    flat.flatten([]).should.eql []

  it "should flatten scalar array", ->
    flat.flatten([1,2,3]).should.eql [1,2,3]

  it "should flatten nested array", ->
    flat.flatten([[1],[[2]],[[[3]]]]).should.eql [1,2,3]

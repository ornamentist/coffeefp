invoke = require "../src/invoker"
_      = require "../src/prelude"


describe "reverser", ->
  ITEMS = [[1,2,3]]

  it "should not invoke on missing array", ->
    _.map(null, invoke.reverser).should.eql []

  it "should invoke array reverse", ->
    _.map(ITEMS, invoke.reverser).should.eql ITEMS



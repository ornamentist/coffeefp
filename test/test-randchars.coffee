chars = require "../src/randchars"
_     = require "../src/prelude"


describe "string", ->
  char = chars.char

  it "should generate 0 character string", ->
    chars.string(char, 0).should.have.length 0

  it "should generate 1 character string", ->
    chars.string(char, 1).should.have.length 1

  it "should generate character strings", ->
    chars.string(char, 42).should.be.a("string")

  it "should generate letter strings", ->
    chars.string(char, 42).should.match(/[a-z]+/)



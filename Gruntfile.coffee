module.exports = (grunt)->

  # Load Grunt plugins

  grunt.loadNpmTasks "grunt-panda"
  grunt.loadNpmTasks "grunt-mocha-test"
  grunt.loadNpmTasks "grunt-contrib-copy"
  grunt.loadNpmTasks "grunt-contrib-clean"
  grunt.loadNpmTasks "grunt-contrib-coffee"


  grunt.initConfig
    coffee:
      options:
        sourceMap: true

      src:
        expand : true
        src    : ["src/**/*.coffee.*"]
        dest   : "lib"
        ext    : ".js"

      test:
        expand : true
        src    : ["test/**/*.coffee"]
        dest   : "lib"
        ext    : ".js"

    clean:
      build : ["lib/**/*"]
      src   : ["test/**/*.js"]
      tests : ["test/**/*.js"]

    mochaTest:
      test:
        expand : true
        cwd    : "lib/test"
        src    : "*.js"

        options:
          require     : ["chai", "lib/test/helper"]
          timeout     : 3000
          ignoreLeaks : false
          ui          : "bdd"
          reporter    : "min"

    panda:
      options:
        pandocOptions: '-t html5 --smart --section-divs --mathjax --normalize ' +\
                       '--indented-code-classes coffee --highlight-style kate ' +\
                       '--css http://kevinburke.bitbucket.org/markdowncss/markdown.css'

      files:
        expand : true
        cwd    : "src"
        src    : "*.coffee.md"
        dest   : "doc"
        ext    : ".html"


  # Handy Grunt tasks

  grunt.registerTask "build", ["coffee"]

  grunt.registerTask "test", ["build", "mochaTest"]

  grunt.registerTask "html", ["panda"]

  grunt.registerTask "default", ["build", "test"]





[![Build Status](https://travis-ci.org/ornamentist/coffeefp.png)](https://travis-ci.org/ornamentist/coffeefp)


## _Functional Javascript_ in CoffeeScript

This project is a selection of programming examples from the book
[Functional JavaScript][1] by Michael Fogus, re-expressed in [CoffeeScript]
[2]. It's not an exhaustive page-by-page translation of this inspiring book, but a selection of the functions that I found useful in learning
Functional Programming and CoffeeScript.


### Environment

All the examples run under [Node.js][7] with workflows managed by [Grunt][8].


### Literate Code

The CoffeeScript source code here is presented in _literate CoffeeScript_
files ending in `.coffee.md`. The literate format's intermixing of code and
exposition is a good match for learning exercises like this project.


### Disclaimer

The CoffeeScript code in this project is was developed for learning purposes so please don't assume it's production quality, or even idiomatic CoffeeScript.


### Type signatures

I've added Haskell-inspired type signature comments before many of the
CoffeeScript functions in the `src` directory. I find these helpful for
understanding the essential nature of each function but they are essentially ad-hoc so please don't treat them like "real" Haskell type signtures.


### Standard Prelude

To keep the examples independent of any specific functional support library I've created a facade module `src/prelude.coffee.md` which re-exports commonly used functions from [Underscore][3] and [related][4] libraries.


### Testing

The CoffeeScript functions have a modest suite of tests implemented with [mocha][5] and [chai][6].


### HTML Versions

HTML versions of the literate source files are available in the `doc`
directory. These have been created with [pandoc][9] and the [panda][10]
Grunt plugin.


### License

Copyright (c) 2013 Stuart Hungerford. Licensed under the MIT license.


[1]: http://shop.oreilly.com/product/0636920028857.do
[2]: http://coffeescript.org/
[3]: http://underscorejs.org/
[4]: http://documentcloud.github.io/underscore-contrib/
[5]: http://mochajs.org/
[6]: http://chaijs.com/api/bdd/
[7]: http://nodejs.org/
[8]: http://gruntjs.com/
[9]: http://johnmacfarlane.net/pandoc/
[10]: https://github.com/gmp26/grunt-panda

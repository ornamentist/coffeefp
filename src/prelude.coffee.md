## Preamble

    underscore = require "underscore"
    ustring    = require "underscore.string"
    ucontrib   = require "underscore-contrib"


### Coffeescript Prelude

For the purposes of these exercises I've wrapped Underscore and its related
libraries into a CoffeeScript facade "prelude" module. Prelude module exports follow a
common naming scheme.


#### Type testing

    exports.is_array    = underscore.isArray
    exports.is_function = underscore.isFunction
    exports.is_NaN      = underscore.isNaN
    exports.is_number   = underscore.isNumber
    exports.is_string   = underscore.isString


#### Type conversion

    exports.to_array  = underscore.toArray
    exports.to_object = underscore.object


#### Value testing

    exports.exists   = ucontrib.exists
    exports.falsey   = ucontrib.falsey
    exports.is_empty = underscore.isEmpty
    exports.truthy   = ucontrib.truthy


#### Collection manipulation

    exports.cat      = ucontrib.cat
    exports.clone    = underscore.clone
    exports.compact  = underscore.compact
    exports.cons     = ucontrib.cons
    exports.count_by = underscore.countBy
    exports.each     = underscore.each
    exports.every    = underscore.every
    exports.filter   = underscore.filter
    exports.find     = underscore.find
    exports.first    = underscore.first
    exports.fold     = underscore.reduce
    exports.foldr    = underscore.reduceRight
    exports.initial  = underscore.initial
    exports.map      = underscore.map
    exports.mapcat   = ucontrib.mapcat
    exports.pick     = underscore.pick
    exports.pluck    = ucontrib.pluck
    exports.reduce   = underscore.reduce
    exports.rest     = underscore.rest
    exports.second   = ucontrib.second
    exports.some     = underscore.some
    exports.take     = ucontrib.first


#### Object manipulation

    exports.result = underscore.result
    exports.pairs  = underscore.pairs


#### String manipulation

    exports.capitalize = ustring.capitalize
    exports.sreverse   = ustring.reverse


#### Function composition

    exports.compose  = underscore.compose
    exports.curry    = ucontrib.curry
    exports.partial  = underscore.partial
    exports.times    = underscore.times
    exports.pipeline = ucontrib.pipeline


#### Combinators

    exports.always   = ucontrib.always
    exports.identity = underscore.identity


#### Boolean operations

    exports.not = ucontrib.not


#### Arithmetic operations

    exports.abs    = Math.abs
    exports.random = underscore.random


#### Trampolines

    exports.trampoline = ucontrib.trampoline


#### Fluent interfaces

    exports.chain = underscore.chain

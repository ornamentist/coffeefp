### Preamble

    _ = require "./prelude"

### Reference

_Functional JavaScript_ by Michael Fogus


### Object Immutability

Immutable data structures are one of the core attributes of popular
functiona programming environments. In the JavaScript environment
immutability of objects is trickier. In Chapter 7 of _Functional
JavaScript_ the author provides guidelines for an immutable approach to
objects:

- Accept values at construction time then remain immutable

- Functions on objects return new objects

Consider a class maintaining a FIFO queue where we provide immutable
constructor (using _defensive cloning_) and enqueuing functions:

    class Queue

      constructor: (items) ->
        @_items = _.clone(items)

      enqueue: (item) ->
        new Queue(_.cat(@_items, [item]))


    q1 = new Queue([1,2,3])
    q2 = q1.enqueue(4)


While `Queue` instances are effectively immutable, a client could easily
access the "private" `_items` array or even access the `Queue` prototype
and change the immutability guarantee. In addition, clients must remember
to use the `new` keyword when creating queues--or we need to make the
`Queue` constructor more robust.

We can go some way to avoiding these issues by making the queue less
object-like and more function-like:


    # queue :: [a] -> [a]

    @queue = (items = []) ->
      _.cat([], _.clone(items))


    # enqueue :: [a] -> a -> [a]

    @enqueue = (queue, item) ->
      _.cat(queue, [item])


Although we have lost the ability to perform object "dot" calls, e.g.
`q2 = q1.enqueue(42)` we have gained simpler, more flexible and more
composable queue functions. In practice, clients of a large or complex
API may value chaining dot calls over composability so consider the
expectations of your API users before taking this approach.





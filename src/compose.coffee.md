### Preamble

    _ = require "./prelude"


### Reference

_Functional JavaScript_ by Michael Fogus


### Function Composition

Chapter 5 of _Functional JavaScript_ is a rich source of insights into
Functional Programming techniques: after dealing with currying and
partial application the author turns to _function composition_. He
points out that JavaScript (and other) developers already use function
composition, as in this expression for testing when a value is not a
string:


    _.not(_.is_string(42))


Of course CoffeeScript has a perfectly good `is not` operator but I'm
using the prelude `not` function to reinforce the functional nature of
this idea. Function composition in this style is performed from the most
nested expression first. By contrast, the prelude `compose` function
performs the composition from right to left in a much more _declarative_
fashion:


    # not_string :: a -> boolean

    @not_string = _.compose(_.not, _.is_string)


More usefully, the `mapcat` function can also be defined compositionally,
provided we have a function to wrap CoffeeScript's "splatting" of
arguments:


    # splat :: (a -> a) -> ([a] -> a)

    splat = (fn) ->
      (args) -> fn(args...)


    # mapcat :: [a] -> (a -> a) -> [a]

    @mapcat = _.compose(splat(_.cat), _.map)




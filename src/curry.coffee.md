### Preamble

    invoker = require "./invoker"
    _       = require "./prelude"


### Reference

_Functional JavaScript_ by Michael Fogus


### Left and Right Currying

In Chapter 5 of _Functional JavaScript_ the author presents techniques
for _currying_ and _partial application_ of functions. He shows that
functions can be curried from the right (last to first argument order)
or from the left (first to last argument order):

    # div_left :: number -> (number -> number)

    div_left = (n) ->
      (d) -> n / d


    # div_right :: number -> (number -> number)

    div_right = (d) ->
      (n) -> n / d


These two curried division functions are sensitive to argument order and
expect their arguments in left-to-right and right-to-left order
respectively. In the rest of the book the author settles on currying
from the right, which is more useful in the JavaScript environment where
functions can accept arbitrary numbers of arguments to the right.

In the same way that functional closures can capture behaviour through
closed-over variables, currying can capture behaviour through fulfilled
function parameters. To see this idea in action we need a more general
currying higher-order function for two parameters:


    # curry2 :: (a -> a) -> (a -> a)

    @curry2 = (fn) ->
      (second) ->
        (first) ->
          fn(first, second)


Now let's setup an array of objects representing a logfile and a
function for formatting logfile entries:


    log = [{IP: "1.2.3.4", time: "1:2:3:4"}...]


    # format :: Entry -> string

    format = (entry) ->
      "#{entry.IP} - #{entry.time}"


We can curry a higher-order IP counting function `count_IP` whose
implementation describes its intent--"count by IP by counting by
formatted entries":


    # count_IP ::

    @count_IP = @curry2(_.count_by)(format)


So why stop at 2 curried parameters? Why not develop three, four or `n`
parameter currying functions? The author points to his experience of
coping with Javascript functions that can accept an arbitrary number of
parameters causing confusion over which parameters have been curried and
which are still unused. By contrast, in functional languages like
Haskell, currying is built in to the language and library APIs are
designed to consistently exploit this feature.




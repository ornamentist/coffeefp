### Preamble

    _ = require "./prelude"


### Reference

_Functional JavaScript_ by Michael Fogus


### First look at JavaScript closures

In Chapter 3 the author of _Functional Javascript_ takes us through the
twists and turns of using closures in JavaScript (and by extension
CoffeeScript). Here's a use of a function acting as a closure that
creates and maintains a private "closed-over" value `capture`:


    # pingpong :: () -> {(number -> number), (number -> number)}

    @pingpong = () ->
      capture = 0

      inc: (n) -> capture += n
      dec: (n) -> capture -= n



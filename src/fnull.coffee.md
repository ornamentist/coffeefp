### Preamble

    _ = require "./prelude"


### Reference

_Functional JavaScript_ by Michael Fogus


### Circumvent Null Arguments

In Chapter 4 of _Functional JavaScript_ the author highlights the common
situation of a useful function that may be given unwanted `null`
arguments. The `fnull` higher-order function cirumvents this situation
by calling the function only if its arguments are safely non-null.

`fnull` makes use of a `useable` function that combines an array of
arguments and defaults into a "useable" array of (hopefully) non-null
values:


    # useable :: [a] -> [a] -> [a]

    useable = (args, defaults) ->
      _.map(args, (a, i) -> a ? defaults[i])


    # fnull :: (a -> a) -> [a...] -> (any... -> any)

    @fnull = (fn, defaults...) ->
      (args...) ->
        fn(useable(args, defaults)...)


Here's a "safe" multiply function that defaults null arguments to 1.
Note that this approach will fail with missing (as opposed to null)
arguments:


    # smultiply :: (number | null) -> (number | null) -> number

    @smultiply = @fnull(((memo, n) -> memo * n), 1, 1)

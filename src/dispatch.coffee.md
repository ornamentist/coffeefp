### Preamble

    invoker = require "./invoker"
    _       = require "./prelude"


### Reference

_Functional JavaScript_ by Michael Fogus


### Function Dispatch

Building on the `invoker` function developed in Chapter 4 of _Functional
JavaScript_ the author uses Chapter 5 to introduce a polymorphic
function dispatcher `dispatch`. `dispatch` accepts a collection of
functions which it invokes in turn until one returns a defined value:


    # dispatch :: [(a -> a)] -> (object -> a... -> a)

    dispatch = (fns...) ->
      (target, args...) ->
        for fn in fns
          result = fn.apply(fn, _.cons(target, args))
          return result if result?


Here's an application of `dispatch` to polymorphically reverse either
JavaScript strings or arrays. Although powerful, this approach
highlights the expressiveness provided by functional languages that
support multi-methods or pattern-matching based function dispatching.


    # to_string :: a -> (string | undefined)

    @to_string = dispatch(
      invoker.invoker("toString", Array.prototype.toString),
      invoker.invoker("toString", String.prototype.toString))


`dispatch` is not limited to using invoker functions--here's a
polymorphic array or string reversing function that relies on a
customized string `reverse` function:


    # sreverse :: string -> (undefined | string)

    sreverse = (string) ->
      if not _.is_string(string)
        undefined
      else
        _.sreverse(string)


    # reverse :: a -> (a | undefined)

    @reverse = dispatch(
      invoker.invoker("reverse", Array.prototype.reverse), sreverse)


We can rely on the searching behaviour of `dispatch` to place a
"sentinel" function for polymorphic search failures. In this case the
sentinel is guaranteed to return a defined result (42):


    # silly :: a -> ([a] | number | string)

    @silly = dispatch(@reverse, _.always(42))



### Preamble

    _ = require "./prelude"


### Reference

_Functional JavaScript_ by Michael Fogus


### Conjoin and Disjoin

In Chapter 6 of _Functional JavaScript_ the author continues to explore
recursive functions by creating new combinator functions for testing the
combined truthiness of _predicate_ arguments.

To support these combinators we need two recursive helper functions:
`every` and `some` that and/or respectively their predicates, but in
such a way that they can _short circuit_ if needed at every recursive
call:


    # every :: [a] -> [a -> a] -> boolean

    every = (args, [fp, rp...]) ->
      return true if not _.exists(fp)

      _.every(args, fp) and every(args, rp)


    # some :: [a] -> [a -> a] -> boolean

    some = (args, [fp, rp...]) ->
      return false if not _.exists(fp)

      _.some(args, fp) or some(args, rp)


Using these helper functions we can define the combinators `conjoin` and
`disjoin`:


    # conjoin :: [a -> a] -> ([a] -> boolean)

    conjoin = (ps...) ->
      (args...) -> every(args, ps)


    # disjoin :: [a -> a] -> ([a] -> boolean)

    disjoin = (ps...) ->
      (args...) -> some(args, ps)


In practice these combinators would be much more effectively implemented
as non-recursive functions (particularly with current JavaScript VM's
lack of support for _tail call optimization_) but they do nicely
illustrate recursive thinking.

As an example let's use these combinators to create declarative
functions for testing evenness/oddness of numbers--but first we will
need some helper functions for single number testing:


    # is_{name} :: number -> boolean

    is_zero = (n) -> n is 0
    is_even = (n) -> n % 2 is 0
    is_odd  = (n) -> n % 2 is 1


    # all_even :: [a] -> boolean

    @all_even = conjoin(_.is_number, is_even)


    # zero_odd :: [a] -> boolean

    @zero_odd = disjoin(is_odd, is_zero)


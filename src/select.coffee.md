### Preamble

    _ = require "./prelude"


### Reference

_Functional JavaScript_ by Michael Fogus


### SQL Inspired Table Functions

Tables and table-like data structures are always with us. In this part
of Chapter 2 of _Functional JavaScript_ the author creates an SQL
(relational algebra) inspired `project` function that both accepts and
returns a table (AKA an array of Javascript objects):


    # project :: [{string, a}] -> [string] -> [{string, a}]

    @project = (table, keys...) ->
      _.map(table, (row) ->
        _.pick(_.cons(row, keys)...))


To move closer to the SQL `SELECT ... AS ...` functionality we need a
table renaming function:


    # rename :: {string, a} -> {string, string} -> {string, a}

    @rename = (row, names) ->
      _.to_object(_.map(row, (value, key) ->
        [names[key] ? key, value]))


Which we can use to create a table `AS` function:


    # as :: [{string, a}] -> {string, string} -> [{string, a}]

    @as = (table, names) ->
      _.map(table, (row) =>
        @rename(row, names))


And for the SQL-like `WHERE` clause we will also need a `restrict`
function that accepts a predicate function:


    # restrict :: [{string, a}] -> ({string, a} -> boolean) -> [{string, a}]

    @restrict = (table, fn) ->
      _.filter(table, (row) -> fn(row))


### To-Do

Create a SQL-inspired `select` function that uses the `project`, `as`
and `restrict` in a functional "pipeline" that processes tables and
returns a table.





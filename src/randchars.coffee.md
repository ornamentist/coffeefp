### Preamble

    _ = require "./prelude"

### Reference

_Functional JavaScript_ by Michael Fogus


### Pure Functions

Chapter 7 of _Functional JavaScript_ opens with a subject central to
any discussion of functional programming: pure functions. The author
defines a _pure function_ as one where:

1. Its return value depends only on its arguments

2. It does not rely on state changed externally to itself

3. It does not change state external to itself

To illustrate this idea consider a function `random` that generates a
random number betweeen 1 and n:


    # random :: number -> number

    random = _.partial(_.random, 1)


While useful, `random` is not a pure function as it violates property 1
(not dependent on its arguments) and property 2 (relies on externally
changed state).

In the JavaScript environment of ubiquitous object references, creating
truly pure functions is effectively impossible--however we can minimize
impurity by clearly separating pure and impure application components.
An application that generates random strings for example, could be
separated into an impure random `char` function and a pure `string`
function:


    # char :: () -> character

    @char = () ->
      random(26).toString(36)


    # string :: (() -> character) -> number -> string

    @string = (generator, n) ->
      _.times(n, generator).join("")


Which we can use to generate distinctly not cryptographcally secure
random passwords:


    password = @string(@char, 16)


Testing and reasoning about `string` is now relatively straightforward,
leaving us to focus on the more unpredictable task of verifying the
impure `char` function.

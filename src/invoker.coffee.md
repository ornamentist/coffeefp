### Preamble

    _ = require "./prelude"


### Reference

_Functional JavaScript_ by Michael Fogus


### Invoke Method by Name

Chapter 4 of _Functional JavaScript_ presents an "invoker" higher-
order function that can invoke a named method on a JavaScript object:


    # invoker :: string -> (a -> a) -> (object -> a... -> a)

    @invoker = (name, method) ->
      (target, args...) ->
        site = target?[name]

        if site and (site is method)
          site.apply(target, args)


Here's an application of `invoker` for reversing array instances:

    # reverser :: [a] -> [a]

    @reverser = @invoker("reverse", Array.prototype.reverse)


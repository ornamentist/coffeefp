### Preamble

    _ = require "./prelude"


### Reference

_Functional JavaScript_ by Michael Fogus


### Recursive Unzip

Chapter 6 of _Functional JavaScript_ tackles recursion (by tackling
recursion, by tackling recursion...), beginning with a `cycle` function
that returns an array of cycled inputs:


    # cycle :: number -> [a] -> [a]

    @cycle = (times, array) ->
      return [] if times <= 0

      _.cat(array, @cycle(times - 1, array))


In this function the recursion terminates when the `times` count reaches
zero and the return value is a concatenated array of values. Flushed
with success, the author now asks what recursive schema we would need to
create an `unzip` function that can "undo" the actions of the prelude
`zip` function?

Let's start with a `pair` helper function that picks out the first and
second elements of a pair and combines them with the rest of a zipped
array. CoffeeScript's ability to _destructure_ arguments is very helpful
here:


    # pair :: [a, a] -> [[a], [a]] -> [[a], [a]]

    @pair = ([f, s], [fr, sr]) ->
      [_.cons(f, fr), _.cons(s, sr)]


Using the `pair` function the recursive `unzip` function becomes
relatively straightforward:


    # unzip :: [[a, a]] -> [[a], [a]]

    @unzip = ([first, rest...]) ->
      return [[], []] if _.is_empty(first)

      @pair(first, @unzip(rest))

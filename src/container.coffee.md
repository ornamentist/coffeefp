### Preamble

    _ = require "./prelude"

### Reference

_Functional JavaScript_ by Michael Fogus


### Mutable Containers

Chapter 7 of _Functional JavaScript_ is a good introduction to the
issues around working with immutable data in the JavaScript environment.
The author highlights the advantages that accrue from working with
immutable data but he recognizes that in practice there will always be
times when we need to mutate JavaScript state.

One way to cope with necessary mutable state is to isolate it into pre-
prepared "containers" that provide a predictable get/set API. Here's an
object-based mutable container:


    class Container

      constructor: (@item) ->

      update: (fn, args...) ->
        @item = fn(_.cons(@item, args)...)


Which we can use like so:


    number = new Container(42)

    number.update((n) -> n + 1)

    number.update(((n, a, b) -> n + a + b), 2, 3)


And here's a function-based, rather than object-based example:


    # container :: a -> {string: a}

    @container = (item) ->
      item: item


    # update :: ([a] -> a) -> [a] -> {string: a}

    @update = (item, fn, args...) ->
      item.item = fn(_.cons(item.item, args)...)
      item



### Preamble

    _ = require "./prelude"


### Reference

_Functional JavaScript_ by Michael Fogus


### The Utility of `foldr`

Volumes have been written on the power and generality of collection
folding functions--particularly `foldr`--but Chapter 2 of _Functional
JavaScript_ introduces the idea nicely with these two functions:

    # all_of :: [a] -> boolean

    @all_of = (items...) ->
      _.foldr(items, ((memo, item) ->
        memo and item), true)


    # any_of :: [a] -> boolean

    @any_of = (items...) ->
      _.foldr(items, ((memo, item) ->
        memo or item), false)


Given the symmetric nature of CoffeeScript's `or` and `and` operators
these two functions could equally well be implemented using a "left"
fold function.



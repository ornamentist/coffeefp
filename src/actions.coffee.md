### Preamble

    _ = require "./prelude"


### Reference

_Functional JavaScript_ by Michael Fogus


### Note

In this example I've diverged from the treatment presented in the text,
and developed examples in the style of [this blog post][1]. I did this
for my own understanding of the topic--you may be better served by the
original presentation, or as they say, YMMV.


### Composing Incompatible Functions

In Chapter 8 of _Functional JavaScript_ the author presents function
_pipelines_ as a powerful approach for composing complex computations.
We learn that each function in a composed pipeline expects that it will
be given data of a predictable format or "shape".

But what if the data shapes flowing between functions are incompatible?
Consider this pair of simple number conversion functions along with a
`log` function that logs the function results for later analysis:


    # {name} :: number -> number

    square = (n) -> n * n
    negate = (n) -> -n


    # log :: number -> string

    log = (n) -> "result: #{n}"


Now suppose we'd like to compose these number functions into a fully
logged computation. Their mutually incompatible data shapes mean our
composition fails:


    # process :: number -> NaN

    process = _.compose(negate, log, square, log)
    result = process(2)


Can we find some mechanism that allows the composition to run without
passing around mutable object references? One approach is to augment the
simple number functions into mutually compatible versions that return
both a result and a log entry:


    # m_{name} :: number -> (number, string)

    @m_square = (n) -> [n*n, "result: #{n*n}"]
    @m_negate = (n) -> [-n,  "result: #{-n}"]


For reasons that will become clear later I've given these functions an
`m_` prefix. What we're missing now is a means of performing functions
like `m_square` and `m_negate` _and_ updating the shared logging trail.
Here's a `bind` function that converts logging number functions into
composable number logging functions:


    # bind :: (number -> (number, [string])) -> ((number, [string]) -> (number, [string]))

    @bind = (f) ->
      ([state, log]) ->
        [answer, message] = f(state)

        [answer, _.cat(log, message)]


Now we can compose our augmented functions as we please:


    # m_process :: (number, [string]) -> (number, [string])

    @m_process = _.compose(@bind(@m_negate), @bind(@m_square))
    result = @m_process([3, []])


The `m_process` composed function still seems clunky though--it needs a
`[3, []]` argument rather than the more natural `3`. Can we do something
about that?  Yes--here's a `unit` function which wraps a numeric
argument in a composable result-message pair:


    # unit :: number -> (number, [string])

    @unit  = (n) -> [n, []]
    result = @m_process(@unit(3))


And finally an aside on nomenclature: the composable functions we've
developed effectively define a [_monad_][2] (specifically the [_writer
monad_][3]). The names `bind`, `unit` and the `m` function prefixes seem
to be popular naming conventions in the [monad literature][4] and I've
followed those conventions here.


[1]: http://blog.jcoglan.com/2011/03/05/translation-from-haskell-to-javascript-of-selected-portions-of-the-best-introduction-to-monads-ive-ever-read/
[2]: http://stackoverflow.com/questions/44965/what-is-a-monad
[3]: http://en.wikipedia.org/wiki/Monads_in_functional_programming#The_Writer_monad
[4]: http://en.wikipedia.org/wiki/Monads_in_functional_programming









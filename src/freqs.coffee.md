### Preamble

    _     = require "./prelude"
    curry = require "./curry"


### Reference

_Functional JavaScript_ by Michael Fogus


### Function Level Immutability

Chapter 7 of _Functional JavaScript_ contains an excellent summary of
the core functional programming principle of _immutable_ data. True
immutability in the highly dynamic JavaScript environment is effectively
impossible, but we can go some way toward it through:

- deep _defensive cloning_ of function state

- deep _defensive freezing_ of function state

- keeping state mutation "function private"

- using an immutable data library--e.g. [mori][1]

None of these approaches is a panacea and all of them may cause
unpredictable interactions with third-party libraries of mutable
objects.

As an example of the composability of immutable state consider a pure
`frequencies` function that builds a table (object) of input frequency
counts:


    # frequencies :: [a] -> {string : number}

    @frequencies = curry.curry2(_.count_by)(_.identity)


`frequencies` takes advantage of the 2-parameter currying function
`curry2` developed in Chapter 5. Since the prelude `identity` function
is pure and the prelude `count_by` function is non-destructive their
composition is pure.

By using the prelude `random` function we can conduct an experiment in
counting coin tosses:


    # tosses :: {string : number}

    @tosses = _.times(1000, _.partial(_.random, 0, 1))

    counts = @frequencies(@tosses)


[1]: http://swannodette.github.io/mori/


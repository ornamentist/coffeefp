### Preamble

    _ = require "./prelude"


### Reference

_Functional JavaScript_ by Michael Fogus


### Do-if-something Idiom

In Chapter 1 the author of _Functional Javascript_ introduces a commonly
used programming idiom for conditionally performing an action, wrapped
in a handy function. Here I'm taking advantage of CoffeeScript's
existential operator `?`:


    # do_when :: boolean -> (a -> a) -> (a | undefined)

    do_when = (condition, action) ->
      if condition? then action() else undefined


He then uses this idiom in a clever function for performing actions
based on the existence of a field in a Javascript collection. The use of
`_.exists` and `_.result` lets us avoid the subtleties of Javascript
object key ownership:


    # do_if_field :: ([a] | {string: a}) -> string -> (a | undefined)

    @do_if_field = (source, name) ->
      do_when _.exists(source[name]), ->
        _.result(source, name)




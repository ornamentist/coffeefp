### Preamble

    _ = require "./prelude"

### Reference

_Functional JavaScript_ by Michael Fogus


### Function Pipelines

Chapter 8 of _Functional JavaScript_ contains a good introduction to
_fluent_ JavaScript libraries built with function call _chaining_. This
approach has its uses, but can we find a more functional approach? The
answer is "yes" if we turn to the concept of a function call _pipeline_,
something like this hypothetical example:


    # pipeline([2,3,null,4], _.compact, _.initial, _.rest)


In this example (which hypothetically yields the value `[3]`) the
starting value is passed down the pipeline to `compact`, then `initial`
and finally `rest` as if we had manually nested those function calls.
Taking our cue from the function nesting analogy we can define a
`pipeline` function that _reduces_ a seed value over the pending
functions:


    # pipeline :: a -> [(a -> a)] -> a

    @pipeline = (seed, fns...) ->
      _.reduce(fns, ((memo, fn) -> fn(memo)), seed)


Using `pipeline` is similar to our hypothetical example:


    one  = @pipeline(1)

    two  = @pipeline(1, (n) -> n + 1)

    four = @pipeline(1, ((n) -> n + 1), ((n) -> n * 2))


At first glance our pipelines seem similar to the lazy chained function
calls introduced earlier in the chapter, but there is an important
distinction: functional constructs like `pipeline` work with immutable
_values_, rather than _references_ to mutable objects as assumed by call
chaining.

The chaining example was also extended to become _lazy_ (computations
performed only when needed), but we can also make pipelines lazy by the
simple expedient of wrapping them in a _thunk_ (pending function call
plus arguments):


    # times_2 :: number -> number

    times_2 = (n) =>
      @pipeline(n, ((n) -> n + 1), ((n) -> n * 2))

    six = times_2(2)


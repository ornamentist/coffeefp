### Preamble

    _      = require "./prelude"
    bounce = require "./trampoline"


### Reference

_Functional JavaScript_ by Michael Fogus


### Generators

Following the introduction of trampoline functions in Chapter 6 of
_Functional JavaScript_, the author develops a basic approach for
handling infinite streams of data using "lazy" evaluation. JavaScript
has no explicit support for laziness but we can go some way towards
emulating it by defining a _lazy sequence_ as a _head_ item followed by
a (recursive) function that calculates the _tail_ items as needed.

Packaging this idea into a JavaScript object we have a kind of
_generator_ of infinite data that generates new items on demand:


    # generator :: a -> (a -> a) -> (a -> a) -> {a}

    generator = (seed, next) ->
      head: seed
      tail: -> generator(next(seed), next)


We will also need two helper functions for accessing a generator's head
and tail:


    # head :: generator -> a

    head = (generator) -> generator.head


    # tail :: generator -> generator

    tail = (generator) -> generator.tail()


And here's a generator that generates successive integers:


    # ints :: generator

    @ints = generator(0, (n) -> n+1)


Accessing the head of the sequence is a simple `head(@ints)` expression,
but accessing the tail is more tedious--we need to build a nested call
structure:


    three = tail(tail(tail(@ints)))


What we need is a counterpart to the prelude `take` function that can
build the nested tail call structure for us. To avoid blowing the stack
we can use a _trampoline function_ introduced earlier in Chapter 6:


    # take_ints :: number -> generator -> [number] -> [number]

    take_ints = (n, generator, result) ->
      return result if n is 0

      h = head(generator)
      t = tail(generator)

      _.partial(take_ints, n - 1, t, _.cat(result, h))


    # take :: number -> generator -> [number]

    @take = (n, generator) ->
      bounce.trampoline(take_ints, n, generator, [])


Here the `take` function simply applies a trampoline function to the
recursive `take_ints` function which manages the array of integers as
they are generated.

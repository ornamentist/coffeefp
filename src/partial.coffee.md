### Preamble

    _ = require "./prelude"


### Reference

_Functional JavaScript_ by Michael Fogus


### Function Dispatch

Currying and partial function application are often confused--
particularly in imperative languages where both techniques need to be
provided by add-on libraries. In Chapter 5 of _Functional JavaScript_
the author draws a clear distinction between the two techniques and in
this example I'll look at his presentation of partial application.

Let's revisit the curried division functions from earlier in Chapter 5,
but this time in partial application form:


    # div_part :: number -> (number -> number)

    div_part = (n) ->
      (d) -> n / d


This is the same implementation as the earlier `div_left` curried
function and demonstrates the equivalence between curried and partial
functions that can only accept one more argument. In practice, however,
partial functions can usefully accept more than one argument at a time--
a property that makes them more convenient to use in JavaScript function
libraries where functions can accept an arbitrary number of "extra"
arguments.

Here's a simple example of a higher-order function that partially
applies its first argument:


    # partial_1 :: (a -> a) -> a -> ([a] -> a)

    partial_1 = (fn, arg) ->
      (args...) ->
        fn.apply(fn, _.cons(arg, args))


And a partially applied divide-by-ten function that uses it:


    # over_10 :: number -> number

    @over_10 = partial_1(((n, d) -> d / n), 10)


Generalizing to partial application of any number of arguments is
straightforward:


    # partial :: (a -> a) -> [a] -> ([a] -> a)

    partial = (fn, args...) ->
      (rest...) ->
        fn.apply(fn, _.cat(args, rest))


Here's `partial` in action with a function that accepts three
multiplicands followed by a final numerator of 10:


    # mult_over_10 :: number -> number -> number -> number

    @mult_over_10 = partial(((a,b,c,d) -> d / a*b*c), 10)



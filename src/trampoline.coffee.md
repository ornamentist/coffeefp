### Preamble

    _ = require "./prelude"


### Reference

_Functional JavaScript_ by Michael Fogus


### Trampoline Functions

In Chapter 6 of _Functional JavaScript_ the author highlights one of the
disadvantages of recursive functions: functions can recurse too deeply
and "blow" the call stack. Some languages avoid these stack problems by
providing _tail call optimization_ for correctly structured recursive
functions, but JavaScript is not (yet) one of them.

All is not lost however--a control structure called a _trampoline_ can
be used to capture and "flatten" out chains of recursive calls. Let's
start by looking at two (admittedly contrived) mutually recursive
functions:


    # even :: number -> boolean

    even = (n) ->
      if n is 0
        true
      else
        odd(_.abs(n) - 1)


    # odd :: number -> boolean

    odd = (n) ->
      if n is 0
        false
      else
        even(_.abs(n) - 1)


Testing for the eveness or oddness of large numbers with these functions
is likely to blow the stack on commonly used JavaScript VM's. But what
if we were to wrap the recursive calls in a _partially applied_
function-- something like this:


    # even :: number -> (boolean | (number -> boolean))

    @even = (n) =>
      if n is 0
        true
      else
        _.partial(@odd, _.abs(n) - 1)


    # odd :: number -> (boolean | (number -> boolean))

    @odd = (n) =>
      if n is 0
        false
      else
        _.partial(@even, _.abs(n) - 1)


Now to use `even` and `odd` we would need to manually flatten the
partially applied functions ourselves:


    odd_0 = @odd(0)

    odd_1 = @odd(1)()

    odd_3 = @odd(3)()()()


This procedure quickly becomes tedious so let's use a trampoline
function to automatically flatten the partial functions for us:


    # trampoline :: (a -> a) -> [a] -> a

    @trampoline = (fn, args...) ->
      result = fn(args...)

      while _.is_function(result)
        result = result()

      result


### To-Do

Develop a less clunky iterative structure for `trampoline`.


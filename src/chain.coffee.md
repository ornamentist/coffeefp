### Preamble

    _ = require "./prelude"

### Reference

_Functional JavaScript_ by Michael Fogus


### Lazy Chains

Chapter 8 of _Functional JavaScript_ opens with an introduction to
_chaining_ of functions. Using the [Underscore][1] style chaining
support we can pluck the titles of an array of books in a _fluent_
programming style:


    books = [{title: "foo"}, {title: "baz"}]

    titles = _.chain(books).pluck("title").sort().value()


Here the `chain` function is making the "plain" `books` array chainable
through behind-the-scenes manipulation and the `value` function is
making the results available as a plain array again.

Fluent, chained interfaces certainly have their uses, but as the author
points out, in Underscore they're not _lazy_. In other languages that
offer lazy evaluation the calls to `pluck` and `sort` would not be
performed until `value` is called--i.e. computations are not performed
until they are needed.

So can we have our cake and lazily eat it too?  Here's a `Lazy` class
that maintains a sequence of functions waiting to be called when needed:


    class @Lazy

      constructor: (item) ->
        @calls  = []
        @target = item

      thunk: (name, args) ->
        (target) ->
          target[name].apply(target, args)

      invoke: (name, args...) ->
        @calls.push(@thunk(name, args))
        this

      force: () ->
        _.reduce(@calls,
          ((target, thunk) -> thunk(target)), @target)


In the `Lazy` class the `invoke` method appends a closure of a function
name and its arguments into a _thunk_ for later evaluation. Conversely,
the `force` method reduces over the array of thunks, passing the combined
function results into each:


    lazy = new @Lazy([3,2,1])

    lazy.invoke("sort").invoke("join", "").force()


Admittedly, baroque code like `Lazy` needs to be abstracted into a well
tested library but it does provide lazy, fluent call chaining when it's
absolutely needed.


[1]: http://underscorejs.org/

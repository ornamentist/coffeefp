### Preamble

    _ = require "./prelude"


### Reference

_Functional JavaScript_ by Michael Fogus


### Recursive Array Flattening

Chapter 6 of _Functional JavaScript_ presents a suprisingly subtle
recursive function for "flattening" a nested array:


    # flatten [[a]] -> [a]

    @flatten = (array) =>
      return [array] if not _.is_array(array)

      _.mapcat(array, @flatten)


By using the prelude `mapcat` function with _itself_ `flatten` is
(indirectly) performing mutual recursion. In this case recursion stops
when the function encounters a scalar value and the combined result is
concatenated in the function result. Notice also that we need to use
the CoffeeScript "fat arrow" `=>` to cope with the JavaScript scope
problems caused by the recursive reference.


### Preamble

    _ = require "./prelude"


### Reference

_Functional JavaScript_ by Michael Fogus


### Creating a `mapcat` Function

In Chapter 2 of _Functional JavaScript_ the author defines some
_applicative_ functions, starting with the `cat` (concatentation of
arguments) function. In CoffeeScript we can rely on argument "spreads"
and the existential operator to yield a simpler function:

    # cat :: any... -> [a]

    @cat = (items...) ->
      if items?[0]? then [].concat(items...) else []


We can use `cat` to define a `cons` (construct) function for arrays:


    # cons :: a -> [a] -> [a]

    @cons = (head, tail) ->
      @cat([head], _.to_array(tail))


These functions are all very well, but as the author points out they're
not _applicative_--i.e. functions that receive other functions as arguments.
The very useful `mapcat` function, however, is applicative:


    # mapcat :: (a -> a) -> [a] -> [a]

    @mapcat = (fn, items) ->
      @cat(_.map(items, fn)...)


If we define another useful function `but_last`--which returns everything
except the last array item--we can combine everything into a general
`interpose` function:


    # but_last :: [a] -> [a]

    but_last = (items) ->
      _.to_array(items).slice(0, -1)


    # interpose :: a -> [a] -> [a]

    @interpose = (item, items) ->
      join = (e) => @cons(e, [item])

      but_last(@mapcat(join, items))



### Preamble

    _ = require "./prelude"


### Reference

_Functional JavaScript_ by Michael Fogus


### Finding the Best Item in an Array

Chapter 4 of _Functional Javascript_ revisits the idea of finding the
"best" item (e.g. minima and maxima) of an array of values. Many
languages (including JavaScript) provide built-in `max()` and `min()`
functions but they're not as generic as they could be. Here's a higher-
order `best` function that is parameterized over the choice of value-
comparing function:


    # best :: (a -> a) -> (a -> a) -> [a] -> a

    @best = (better, items) ->
      _.reduce(items, (memo, item) ->
        if better(memo, item) then memo else item)



